const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

txtFirstName.addEventListener('keyup', getFullName);

// Listen to an event when the last name's input is changed.
txtLastName.addEventListener('keyup', getFullName);

/* Instead of anonymous functions for each of the event listener:
 - Create a function that will update the span's contents based on the value of the first and last name input fields.
 - Instruct the event listeners to use the created function.
*/
function getFullName(){	
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
}
